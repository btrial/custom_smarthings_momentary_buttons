A list of momentary capabilities, presentations and DataHandlers for New Smartthings APP.

<img src="dashboardView.png" height="400" alt="dashboardView"/>
<img src="detailView.png" height="400" alt="detailView"/>

DataHandlers expose ON/OFF switch events (to webcore too), but they do not keep ON/OFF state, but get back to "undefined" switch state imediatelly in order to be able to push momentary press again.

1) momentaryButtonEnable - momentary capability and presentation for enable tile
2) momentaryButtonDisable - momentary capability and presentation for disable tile
3) lastRunningDateTime -  capability and presentation for last running dateTime tile
4) singleMomentaryButton - single momentary button DTH using 1) 3) for on/lastRun tiles
5) doubleMomentaryButton - double momentary button DTH using 1) 2) 3) for on/off/lastRun tiles
6) doubleBroadlinkMomentaryButton - double momentary button DTH using 1) 2) 3) for on/off/lastRun tiles, to control Broadlink devices
7) doubleHTTPMomentaryButton - double momentary button DTH using 1) 2) 3) for on/off/lastRun tiles, to send HTTP requests

Installation:
* create new DataHandler in Smartthings IDE using one code from one of dataHandler.txt files from 4) 5) 6) 7)
OR
* create your new DataHandler using capabilitie and presentation from 1) 2) 3)
